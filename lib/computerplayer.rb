require_relative 'board'
require_relative 'battleship'

class ComputerPlayer

  def initialize (board = Board.new)
    @board = board
  end

  attr_reader :board

  def get_play (board)
    board.random_shot
  end

  def set_ship
    board[board.random_empty_position] = :s
  end

end
