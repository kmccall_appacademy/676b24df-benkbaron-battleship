require_relative 'computerplayer'
require_relative 'player'

require 'byebug'
class Board

  def initialize (grid = nil)
    grid ? @grid = grid : @grid = Board.default_grid
  end

  attr_reader :grid

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    return self[pos] == nil if pos
    grid.flatten.all? { |el| el == nil }
  end

  def full?
    grid.flatten.include?(nil) == false
  end

  def won?
    grid.flatten.include?(:s) == false
  end

  def add_full_ship(ship)
    pos = random_empty_position

  if room_for_ship_ver?(ship, pos)
    add_ver_ship(ship, pos)

    elsif room_for_ship_hor?(ship, pos)
      add_hor_ship(ship, pos)

    # elsif room_for_ship_ver?(ship, pos)
    #   add_ver_ship(ship, pos)

    else
      add_full_ship(ship)
    end
  end


  def place_random_ship
    raise if full?
    self[random_empty_position] = :s
  end

  def random_empty_position
    pos = []
    pos << Random.new.rand(grid.length)
    pos << Random.new.rand(grid[0].length)

    self[pos] == nil ? pos : random_empty_position
  end

  def random_shot
    pos = []
    pos << Random.new.rand(grid.length)
    pos << Random.new.rand(grid[0].length)
    if self[pos] == :x || self[pos] == :H
      random_shot
    else
      pos
    end
  end

  def [] (pos)
    x, y = pos
    @grid[x][y]
  end

  def []= (pos, mark)
    x, y = pos
    @grid[x][y] = mark
  end

  private

  def add_hor_ship(ship, pos)
    x, y = pos
    i = 0
    while i < ship.length
      grid[x][y + i] = :s
      i += 1
    end
  end

  def add_ver_ship(ship, pos)
    x, y = pos
    i = 0
    while i < ship.length
      grid[x + i][y] = :s
      i += 1
    end
  end

  def room_for_ship_hor?(ship, pos)
    x, y = pos
    i = 0
    while i < ship.length
      return false unless grid[x][y + i] == nil && y + i < grid[0].length
      i += 1
    end
    true
  end

  def room_for_ship_ver?(ship, pos)
    #debugger
    x, y = pos
    i = 0
    while i < ship.length
      return false unless x + i < grid.length && grid[x + i][y] == nil
      i += 1
    end
    true
  end

end
