require_relative 'player'
require_relative 'board'
require_relative 'computerplayer'
require_relative 'ship'

class BattleshipGame

  def initialize (player1, board, player2 = nil, board2 = nil)
    @player1 = player1
    @player2 = player2
    @board = board
    @board2 = board2
    @current_player = player1
  end

  attr_reader :player1, :player2, :board2, :current_player

  def board
    return @board if board2.nil?
    current_board
  end

  def current_board
    current_player == player1 ? @board2 : @board
  end

  def attack(pos)
    if board[pos] == :s || board[pos] == :H
       board[pos] = :H
    else
      board[pos] = :x
    end
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    display
    pos = current_player.get_play(board)
    attack(pos)
    display
    switch_players
  end

  def play
    setup
    until game_over?
      play_turn
    end
  end

  def setup
    Ship.all_ships.each do |ship|
      player1.board.add_full_ship(ship)
    end
    
    Ship.all_ships.each do |ship|
      player2.board.add_full_ship(ship)
    end

  #  10.times { player1.set_ship }
  #  10.times { player2.set_ship }
  end

private

  def switch_players
    return player1 if player2 == nil

    if current_player == player1
      @current_player = player2
    else
      @current_player = player1
    end
  end

  def display
    puts "#{current_player}"
    board.grid.each do |row|
      arr = []
      row.each do |el|
        if el == :s
          arr << nil
        else
          arr << el
        end
      end
      p arr
    end
  end

end


if $PROGRAM_NAME == __FILE__
  ben = HumanPlayer.new
  # 40.times {ben.board.place_random_ship}
  compy = ComputerPlayer.new
  # 40.times {compy.board.place_random_ship}

  game = BattleshipGame.new(ben, ben.board, compy, compy.board)

  game.play
end
