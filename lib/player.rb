require_relative 'board'
require_relative 'battleship'

class HumanPlayer

  def initialize (board = Board.new)
    @board = board
  end

  attr_accessor :board

  def get_play (board = nil)
    get_coord
  end

  def set_ship
    board[get_coord] = :s
  end

  def get_coord
    pos = []
    puts "enter x coord"
    x = gets.chomp
    puts "enter y coord"
    y = gets.chomp
    pos << x.to_i
    pos << y.to_i
    pos
  end

end
